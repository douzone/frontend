import { createAction, handleActions } from "redux-actions";

import { Map } from "immutable";
import { pender } from "redux-pender";

import * as api from "lib/api";

const INITIALIZE = "add/INITIALIZE";
const CHANGE_INPUT = "add/CHANGE_INPUT";
const WRITE_ADD = "add/WRITE_ADD";

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeAdd = createAction(WRITE_ADD, api.writeAdd);

const initialState = Map({
    no: "",
  name: "",
  startDay: "",
  endDay: "",
  title: "",
  gubun: "",
  content: ""
});

export default handleActions(
  {
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
      const { name, value } = action.payload;
      return state.set(name, value);
    }
  },
  initialState
);
