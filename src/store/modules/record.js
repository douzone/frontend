import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';

const INITIALIZE = 'record/INITIALIZE';
const CHANGE_INPUT = 'record/CHANGE_INPUT';
const RECORD_LIST = 'record/RECORD_LIST';
const SEARCH_RECORD_LIST = 'record/SEARCH_RECORD_LIST';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const getRecordList = createAction(RECORD_LIST, api.getRecordList);
export const getSearchRecordList = createAction(SEARCH_RECORD_LIST, api.getSearchRecordList);

const initialState = Map({
    day: '',
    actor: '',
    id: '',
    recordType: '',
    content: '',
    read: '',
    tables: List(),
    startdate: '',
    enddate: '',
    name: '',
    badge: ''
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const {name, value} = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: RECORD_LIST,
        onSuccess: (state, action) => {
            console.log(action.payload);

            const { data : tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: SEARCH_RECORD_LIST,
        onSuccess: (state, action) => {
            console.log(action.payload);

            const {data : tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    })
}, initialState);

