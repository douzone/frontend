import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from '../../lib/api';

const CHANGE_INPUT = 'list/CHANGE_INPUT';
const TABLE_LIST = 'list/TABLE_LIST';
const FULL_TABLE_LIST = 'list/FULL_TABLE_LIST';
const CLICK = 'list/CLICK';


export const changeInput = createAction(CHANGE_INPUT);
export const getTableList = createAction(TABLE_LIST, api.getTableList);
export const getFullTableList = createAction(FULL_TABLE_LIST, api.getFullTableList);
export const click = createAction(CLICK);

const initialState = Map({
    startdate: '',
    enddate: '',
    name: '',
    no: 0,
    tables: List(),
    lastPage: null
});

export default handleActions({
    [CHANGE_INPUT]: (state, action) => {
        const { name, value} = action.payload;
        return state.set(name, value);
    },
    [CLICK]: (state, action) => {
        const value = action.payload;
        return state.set('no', value);
    },
    ...pender({
        type: TABLE_LIST,
        onSuccess: (state, action) => {
            console.log(action.payload);
            const { data: tables } = action.payload.data;
            
            const lastPage = action.payload.headers['last-page'];
            return state.set('tables', fromJS(tables))
                        .set('lastpage', parseInt(lastPage, 10));
        }
    }),
    ...pender({
        type: FULL_TABLE_LIST,
        onSuccess: (state, action) => {
            console.log(action.payload);
            const{data: tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    })
}, initialState);