import { createAction, handleActions } from 'redux-actions';

import { Map, List} from 'immutable';
import { pender } from 'redux-pender';

import * as api from '../../lib/api';

const TABLE_LIST = 'list/CALENDAR_LIST';

export const getList = createAction(TABLE_LIST, api.getCalendarList);

const initialState = Map({
    tables: List()
});

export default handleActions({
    ...pender({
        type: TABLE_LIST,
        onSuccess: (state, action) => {
            return state.set('calList', action.payload.data);
        }
    })
}, initialState);