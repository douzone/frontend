import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';

const INITIALIZE = 'holiday/INITIALIZE';
const CHANGE_INPUT = 'holiday/CHANGE_INPUT';
const WRITE_HOLI_DAY = 'holiday/WRITE_HOLI_DAY';
const HOLI_DAY_LIST = 'holiday/HOLI_DAY_LIST';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeHoliDay = createAction(WRITE_HOLI_DAY, api.writeHoliDay);
export const getHoliDayList = createAction(HOLI_DAY_LIST, api.getHoliDayList);

const initialState = Map({
    day: '',
    use: 'true',
    description: '',
    tables: List()
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: WRITE_HOLI_DAY,
        onSuccess: (state, action) => {
            console.log(action.payload);

            return;
        }
    }),
    ...pender({
        type: HOLI_DAY_LIST,
        onSuccess: (state, action) => {
            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    })
}, initialState);