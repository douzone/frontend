import {createAction, handleActions} from 'redux-actions';

import {Map, fromJS} from 'immutable';
import {pender} from 'redux-pender';

import * as api from '../../lib/api';

const INITIALIZE = 'pagination/INITIALIZE';
const GET_PAGE = 'pagination/GET_PAGE';
const GET_SEARCH_PAGE = 'pagination/GET_SEARCH_PAGE';
const PAGE_CHANGE = 'pagination/PAGE_CHANGE';
const GET_WORKTIME_TOTAL = 'worktime/GET_WORKTIME_TOATAL';
const GET_BREAKTIME_TOTAL = 'breaktime/GET_BREAKTIME_TOTAL';
const GET_HOLIDAY_TOTAL = 'holiday/GET_HOLIDAY_TOTAL';
const GET_RECORD_TOTAL = 'record/GET_RECORD_TOTAL';
const GET_SEARCH_RECORD_PAGE = 'record/GET_SEARCH_RECORD_PAGE';

export const initialize = createAction(INITIALIZE);
export const pageChange = createAction(PAGE_CHANGE);
export const getPage = createAction(GET_PAGE, api.getPage);
export const getSearchPage = createAction(GET_SEARCH_PAGE, api.getSearchPage);
export const getWorkTimeTotal = createAction(GET_WORKTIME_TOTAL, api.getWorkTimeTotal);
export const getBreakTimeTotal = createAction(GET_BREAKTIME_TOTAL, api.getBreakTimeTotal);
export const getHoliDayTotal = createAction(GET_HOLIDAY_TOTAL, api.getHoliDayTotal);
export const getRecordTotal = createAction(GET_RECORD_TOTAL, api.getRecordTotal);
export const getSearchRecordPage = createAction(GET_SEARCH_RECORD_PAGE, api.getSearchRecordPage);

const initialState = Map({
    activePage: 1,
    totalCount: 0
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [PAGE_CHANGE]: (state, action) => {
        const {activePage, pageNumber} = action.payload;

        console.log("page change : " + activePage + " " + pageNumber);

        return state.set('activePage', pageNumber);
    },
    ...pender({
        type:GET_PAGE,
        onSuccess: (state, action) => {
            console.log("페이징 : " + action.payload);

            const totalCount = action.payload.data.data;
            console.log("totalCount : " + totalCount);

            return state.set('totalCount', fromJS(totalCount));
        }
    }),
    ...pender({
        type: GET_SEARCH_PAGE,
        onSuccess: (state, action) => {
            console.log("GET_SEARCH_PAGE : " + action.payload);

            const totalCount = action.payload.data.data;
            console.log("totalCount : " + totalCount);

            return state.set('totalCount', fromJS(totalCount));
        }
    }),
    ...pender({
        type: GET_WORKTIME_TOTAL,
        onSuccess: (state, action) => {
            console.log("GET_WORKTIME_TOTAL : " + action.payload);

            const totalCount = action.payload.data.data;
            console.log("totalCount : " + totalCount);

            return state.set('totalCount', fromJS(totalCount));
        }
    }),
    ...pender({
        type: GET_BREAKTIME_TOTAL,
        onSuccess: (state, action) => {
            console.log("GET_BREAKTIME_TOTAL : " + action.payload);

            const totalCount = action.payload.data.data;
            console.log("totalCount : " + totalCount);

            return state.set('totalCount', fromJS(totalCount));
        }
    }),
    ...pender({
        type: GET_HOLIDAY_TOTAL,
        onSuccess: (state, action) => {
            console.log("GET_HOLIDAY_TOTAL : " + action.payload);

            const totalCount = action.payload.data.data;
            console.log("totalCount : " + totalCount);

            return state.set('totalCount', fromJS(totalCount));
        }
    }),
    ...pender({
        type: GET_RECORD_TOTAL,
        onSuccess: (state, action) => {
            console.log("GET_RECORD_TOTAL : " + action.payload);

            const totalCount = action.payload.data.data;
            console.log("totalCount : " + totalCount);

            return state.set('totalCount', fromJS(totalCount));
        }
    }),
    ...pender({
        type: GET_SEARCH_RECORD_PAGE,
        onSuccess: (state, action) => {
            console.log("GET_SERARH_RECORD_PAGE : " + action.payload);

            const totalCount = action.payload.data.data;
            console.log("totalCount : " + totalCount);

            return state.set("totalCount", fromJS(totalCount));
        }
    })
}, initialState)

