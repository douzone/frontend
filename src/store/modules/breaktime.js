import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';

const INITIALIZE = 'breaktime/INITIALIZE';
const CHANGE_INPUT = 'breaktime/CHANGE_INPUT';
const WRITE_BREAK_TIME = 'breaktime/WRITE_BREAK_TIME';
const BREAK_TIME_LIST = 'breaktime/BREAK_TIME_LIST';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeBreakTime = createAction(WRITE_BREAK_TIME, api.writeBreakTime);
export const getBreakTimeList = createAction(BREAK_TIME_LIST, api.getBreakTimeList);

const initialState = Map({
    start: '00:00',
    end: '00:00',
    use: 'true',
    description: '',
    tables: List()
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: WRITE_BREAK_TIME,
        onSuccess: (state, action) => {
            const { start, end, use, description } = action.payload.data;
            return state.set('start', start)
                        .set('end', end)
                        .set('use', use)
                        .set('description', description);
        }
    }),
    ...pender({
        type: BREAK_TIME_LIST,
        onSuccess: (state, action) => {
            console.log(action.payload);

            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    })
}, initialState);