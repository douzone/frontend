import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';

const INITIALIZE = 'worktime/INITIALIZE';
const CHANGE_INPUT = 'worktime/CHANGE_INPUT';
const WRITE_WORK_TIME = 'worktime/WRITE_WORK_TIME';
const WORK_TIME_LIST = 'worktime/WORK_TIME_LIST';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeWorkTime = createAction(WRITE_WORK_TIME, api.writeWorkTime);
export const getWorkTimeList = createAction(WORK_TIME_LIST, api.getWorkTimeList);

const initialState = Map({
    start: '00:00',
    end: '00:00',
    use: 'true',
    tables: List()
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: WRITE_WORK_TIME,
        onSuccess: (state, action) => {
            console.log(action.payload);

            const { start, end, use } = action.payload.data;
            return state.set('start', start)
                        .set('end', end)
                        .set('use', use);
        }
    }),
    ...pender({
        type: WORK_TIME_LIST,
        onSuccess: (state, action) => {
            console.log(action.payload);

            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    })
}, initialState);