import {createAction, handleActions} from 'redux-actions';

import {Map, fromJS} from 'immutable';
import {pender} from 'redux-pender';

import * as api from '../../lib/api';

const GO_TO = 'commute/GO_TO';
const GO_OFF = 'commute/GO_OFF';
const STATE_CHANGE = 'commute/STATE_CHANGE';

export const goTo = createAction(GO_TO, api.goTo);
export const goOff = createAction(GO_OFF, api.goOff);
export const stateChange = createAction(STATE_CHANGE);

const initialState = Map({
    goto: false,
    gooff: false,
    gotoDisabled: false,
    gotoVariant: "primary",
    offDisabled: true,
    offVariant: "secondary"
});

export default handleActions({
    [STATE_CHANGE]: (state, action) => {
        const {name, value} = action.payload;

        console.log("STATE_CHANGE : " + name + " " + value);

        return state.set(name, value);
    },
    ...pender({
        type: GO_TO,
        onSuccess: (state, action) => {
            console.log("GO_TO : " + action.payload);

            if(action.payload.data.data == false){
                alert("이미 출근하셨습니다.");
            }
            else {
                localStorage.setItem('goto', true);
                localStorage.setItem('gooff', false);

                return state.set('gotoDisabled', true)
                    .set('gotoVariant', 'secondary')
                    .set('offDisabled', false)
                    .set('offVariant', 'primary');
            }

            return state.set('goto', true)
                        .set('gooff', false);
        }
    }),
    ...pender({
        type: GO_OFF,
        onSuccess: (state, action) => {
            console.log("GO_OFF : " + action.payload);

            if(action.payload.data.data == false) {
                alert("이미 퇴근하셨습니다.");
            }
            else {
                 localStorage.removeItem('goto');
                 localStorage.removeItem('gooff');
                //localStorage.clear();

                return  state.set('gotoDisabled', false)
                    .set('gotoVariant', 'primary')
                    .set('offDisabled', true)
                    .set('offVariant', 'secondary');
            }

            return state.set('gooff', true)
                .set('goto', false);
        }
    })
}, initialState);

