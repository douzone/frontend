import { AUTH_LOGIN, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILURE, AUTH_LOGOUT } from '../../LoginActionTypes';

const initialState = {
    login: {
        status: 'INIT'
    },
    logout: {
      status: 'INIT'
    },
    status: {
        valid: false,
        isLoggedIn: false,
        currentUser: ''
    }
}

// dispatch 함수로부터 전달받은 action 객체의 type에 따라 state를 변경
export default function authentication(state = initialState, action) {
  console.log('action 객체의 type에 따라 상태 변경');
  switch (action.type) {
    case AUTH_LOGIN:
      console.log('AUTH_LOGIN일 경우');
      console.log('');
      return {
        ...state,
        login : {
            status: 'WAITING'
        },
        logout : {
            status: 'INIT'
        }
      };
    case AUTH_LOGIN_SUCCESS:
    console.log('AUTH_LOGIN_SUCCESS일 경우');
    console.log('');
      return {
        ...state,
        login : {
            status: 'SUCCESS'
        },
        logout : {
            status: 'INIT'
        },
        status: {
            ...state.status,
            isLoggedIn: true,
            currentUser: action.user
        }
      };
    case AUTH_LOGIN_FAILURE:
    console.log('AUTH_LOGIN_FAILURE일 경우');
    console.log('');
      return {
        ...state,
        login: {
            status: 'FAILURE'
        },
        logout : {
            status: 'INIT'
        }
      };
    case AUTH_LOGOUT:
    console.log('AUTH_LOGOUT일 경우');
    console.log('');
      return {
        login: {
          status: 'FAILURE'
        },
        logout: {
          status: 'SUCCESS'
        },
        status: {
          ...state.status,
          isLoggedIn: false,
          currentUser: ''
        }
      }
    default:
        return state;
    }
}
