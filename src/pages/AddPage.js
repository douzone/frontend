import React from "react";
import AddContainer from "containers/add/AddContainer";

const AddPage = () => {
  return (
    <div>
      <AddContainer />
    </div>
  );
};

export default AddPage;
