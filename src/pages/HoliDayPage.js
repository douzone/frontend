import React from 'react';
import HoliDayContainer from 'containers/service/HoliDayContainer';
import HoliDayListContainer from 'containers/service/HoliDayListContainer';
import HoliDayPaginationContainer from 'containers/service/HoliDayPaginationContainer';

const HoliDayPage = () => {
    return (
        <div>
            <HoliDayContainer/>
            <HoliDayListContainer/>
            <HoliDayPaginationContainer/>
        </div>
    );
};

export default HoliDayPage;