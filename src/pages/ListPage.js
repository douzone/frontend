
import ListSearchContainer from 'containers/list/ListSearchContainer';
import LIstTableContainer from 'containers/list/LIstTableContainer';
import PaginationContainer from "../containers/list/PaginationContainer";
import CalendarContainer from "../containers/list/CalendarContainer.js";
import "./ListPage.css"
import React, { Component } from "react";

class ListPage extends Component {
    state = {value:0};

    changeValue=(a)=>{
        console.log(a);
       this.setState({
           value:a
       })
    }

    componentWillMount(){
        this.setState({
            value:1
        })
    }
    
    render(){
        const {value}=this.state;
    return (
        <div>
            <ListSearchContainer show={(a)=>this.changeValue(a)}/>   
            <div className={value==0?"show-none":""}>
             <CalendarContainer/>
             </div>
             <div className={value==1?"show-none":""}>
            <LIstTableContainer/>
            <PaginationContainer/>
            </div>
              </div>
    );
    }
    componentDidMount(){
        this.setState({
            value:0
        })
    }
};


export default ListPage;