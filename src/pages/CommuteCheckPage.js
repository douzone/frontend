import React from 'react';
import CommuteCheckContainer from 'containers/commute/CommuteCheckContainer';;

const CommuteCheckPage = () => {
    return (
        <div>
            <CommuteCheckContainer/>
        </div>
    );
};

export default CommuteCheckPage;