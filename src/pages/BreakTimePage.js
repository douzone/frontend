import React from 'react';
import BreakTimeContainer from 'containers/service/BreakTimeContainer';
import BreakTimeListContainer from 'containers/service/BreakTimeListContainer';
import BreakTimePaginationContainer from 'containers/service/BreakTimePaginationContainer';

const WorkTimePage = () => {
    return (
        <div>
            <BreakTimeContainer/>
            <BreakTimeListContainer/>
            <BreakTimePaginationContainer/>
        </div>
    );
};

export default WorkTimePage;