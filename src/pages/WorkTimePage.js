import React from 'react';
import WorkTimeContainer from 'containers/service/WorkTimeContainer';
import WorkTimeListContainer from 'containers/service/WorkTimeListContainer';
import WorkTimePaginationContainer from 'containers/service/WorkTimePaginationContainer';

const WorkTimePage = () => {
    return (
        <div>
            <WorkTimeContainer/>
            <WorkTimeListContainer/>
            <WorkTimePaginationContainer/>
        </div>
    );
};

export default WorkTimePage;