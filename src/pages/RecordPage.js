import React from 'react';
import RecordSearchContainer from 'containers/record/RecordSearchContainer';
import RecordListContainer from 'containers/record/RecordListContainer';
import RecordPaginationContainer from 'containers/record/RecordPaginationContainer';


const RecordPage = () => {
    return(
        <div>
            <RecordSearchContainer/>
            <RecordListContainer/>
            <RecordPaginationContainer/>
        </div>
    );
}

export default RecordPage;