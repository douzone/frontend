import axios from 'axios';
import queryString from 'query-string';

//await axios.get("http://localhost:8076/commute/search")
//await axios.get("http://localhost:8076/commute/list/1")
export const getFullTableList = (page) => axios.get("/commute/list/" + page);
//export const getTableList = (search, page) => axios.get('http://jsonplaceholder.typicode.com/posts/1');
export const getTableList = (search, page) => axios.post("/commute/search/", {search, page});

export const getCalendarList =(search)=> axios.get("/commute/calendar/"+search.paramNo+"?start="+search.startdate+"&end="+search.enddate);
export const getSearchUserList = (name) => axios.get("/user/"+name);

export const getPage = () => axios.get("/commute/page");
export const getSearchPage = (startdate, enddate, paramNo) => axios.get("/commute/searchpage/" + paramNo + "?start=" + startdate + "&end=" + enddate);

export const writeWorkTime = ({start, end, userId, use}) => axios.post("/worktime/new", {start, end, userId, use});
export const getWorkTimeList = (page) => axios.get("/worktime/list/" + page);
export const getWorkTimeTotal = () => axios.get("/worktime/page");

export const writeBreakTime = ({start, end, description, userId, use}) => axios.post("/breaktime/new", {start, end, description, userId, use});
export const getBreakTimeList = (page) => axios.get("/breaktime/list/" + page);
export const getBreakTimeTotal = () => axios.get("/breaktime/page");

export const goTo = (no, id, commute, recordType) => axios.post("/work/goto", {no, id, commute, recordType});
export const goOff = (no, id, commute, recordType) => axios.post("/work/gooff", {no, id, commute, recordType});

export const writeHoliDay = ({day, description, userId, use}) => axios.post("/holiday/new", {day, description, userId, use});
export const getHoliDayList = (page) => axios.get("/holiday/list/" + page);
export const getHoliDayTotal = () => axios.get("/holiday/page");

export const writeAdd = ({no,name,startDay,endDay,title,state,workAttitudeList,content,userNo,userId}) => axios.post("/workattitude/new",{no,name,startDay,endDay,title,state,workAttitudeList,content,userNo,userId});

export const getRecordList = (page) => axios.get("/record/list/" + page);
export const getRecordTotal = () => axios.get("/record/page");
export const getSearchRecordList = (page, startdate, enddate, name, content) => axios.get("/record/search/" + page + "?" + "start=" + startdate + "&end=" + enddate + "&name=" + name + "&content=" + content);
export const getSearchRecordPage = (startdate, enddate, name, content) => axios.get(`/record/searchpage?${queryString.stringify({startdate, enddate, name, content})}`);
