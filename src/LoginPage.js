import React, {Component} from 'react';
import Axios from 'axios';
import PropTypes from 'prop-types';

export class LoginPage extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            id: '',
            password: '',
            submitted: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    // thunk를 실행하는 메소드
    // 반환 값이 false이면 비밀번호 입력창 초기화
    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    // 로그인 버튼 클릭시 submitted값 변경
    handleLogin = (e) => {
        // refresh(새로고침) 막음
        e.preventDefault();
        
        this.setState({ submitted: true });
        let id = this.state.id;
        let pw = this.state.password;

        // 로그인 실패일 경우 비밀번호 value 초기화
        this.props.onLogin(id, pw).then(
            (success) => {
                if(!success) {
                    this.setState({
                        password: ''
                    });
                }
            }
        );
    }
    
    // 엔터키로 입력하면 handleKeyPress 함수를 거치고 handleLogin 함수 호출
    handleKeyPress = (e) => {
        if(e.charCode==13) {
            console.log('Enter키로 입력하셨습니다.');
            console.log('');
            this.handleLogin(e);
        }
    }

    render() {
        const { id, password, submitted } = this.state;
        return(
            <div>
                <h2>Login</h2>
                <form 
                onSubmit={this.handleLogin}
                onKeyPress={this.handleKeyPress}
                >
                    <div>
                        <label htmlFor='userid'>Username</label>
                        <input 
                            type='text' 
                            name='id'  
                            value={this.state.id} 
                            onChange={this.handleChange}
                            autoComplete='off' 
                            placeholder='아이디' />
                        {/* 
                        validation 입니다. 
                        */}
                        { submitted && !id &&
                            <div>Id is required</div>
                        }
                    </div>
                    <div>
                        <label htmlFor='password'>Password</label>
                        <input 
                            type='password' 
                            name='password' 
                            value={this.state.password} 
                            onChange={this.handleChange} 
                            placeholder='비밀번호' /> 
                        {/* 
                        validation 입니다. 
                        */}
                        { submitted && !password &&
                            <div>Password is required</div>
                        }
                    </div>
                    <div>
                        <button>Login</button>
                    </div>
                </form>
                {/*
                <div>name {this.state.userid}</div>
                <div>password {this.state.password}</div>
                <div>submitted {this.state.submitted}</div>
                */}
            </div>
        )
    }
}

LoginPage.prototypes = {
    onLogin: PropTypes.func
};

export default LoginPage; 