import React, { Component } from 'react';
import { connect } from 'react-redux';
import { LoginPage } from './LoginPage';
import { loginRequest } from './LoginActions';

class Login extends Component {
    
    handleLogin = (id, pw) => {
        console.log("handleLogin 함수 호출");
        console.log("id: " + id);
        console.log("pw: " + pw);
        console.log('');
        return this.props.loginRequest(id, pw).then(
            () => {
                console.log('로그인 상태 값: ' + this.props.status);
                console.log('');
                if(this.props.status === 'SUCCESS') {
                    let loginData = {
                        isLoggedIn: true,
                        username: id
                    };
                    return true;
                } else {
                    return false;
                }
            }
        );
    }

    render() {
        return(
            <div>
                <LoginPage onLogin={this.handleLogin}/>
            </div>
        );
    }
}

// coneect()를 사용하기 위해 정의하는 함수
// Redux 스토어의 상태를 어떻게 변형할지
// 어떤 속성을 프레젠테이션 컴포넌트로 넘겨줄지
// 로그인 상태 정보를 넘겨준다.
const mapStateToProps = (state) => {
    return {
        status: state.authentication.login.status
    };
};

// 컴포넌트의 특정 함수형 props를 실행 했을 떄,
// 개발자가 지정한 action을 dispatch 한다.
// 로그인하면 loginRequest가 실행한다.
const mapDispatchToProps = (dispatch) => {
    return {
        loginRequest: (id, pw) => {
            return dispatch(loginRequest(id, pw));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);