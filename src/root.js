import React, {Component} from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import App from 'components/App';
import { connect } from 'react-redux';
import Error404 from './error/404';
import Login from './Login';


class Root extends Component {
    render() {
        return (
            <BrowserRouter>
                    {/* 현재 로그인 아닌 상태 */}
                    {/*!this.props.status &&
                        <Route exact path="/login" component = {Login} />
                    */}
                    {/* 현재 로그인 된 상태 */}
                    {/*this.props.status &&
                        <Route path="/" component={App} />
                    */}
                    <App />
                    
            </BrowserRouter>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        status: state.authentication.status.isLoggedIn
    };
};

export default connect(mapStateToProps)(Root);