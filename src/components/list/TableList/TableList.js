import React from 'react';
import { Table } from 'react-bootstrap';

const TableList = ({tables}) => {
    const TableList = tables.map(
        (table, index) => {
            const { name, userNo, day, totalWorktime, state, commute } = table.toJS();
            return (
                <tr key={index}>
                    <td>{userNo}</td>
                    <td>{name}</td>
                    <td>{day}</td>
                    <td>{totalWorktime}</td>
                    <td>{state}</td>
                    <td>{commute}</td>
                </tr>
            )
        }
    );
    return (
        <div>
            <Table responsive hover>
                <thead>
                <tr>
                    <th>User No</th>
                    <th>이름</th>
                    <th>일자</th>
                    <th>총근무시간</th>
                    <th>상태</th>
                    <th>구분</th>
                </tr>
                </thead>
                <tbody>
                    { TableList }
                </tbody>
            </Table>
        </div>
    )
    
};


export default TableList;