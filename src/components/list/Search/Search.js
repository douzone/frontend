import React, {Component} from 'react';
import {Button, Row, Col } from 'react-bootstrap';
import CalendarTableSelect from './CalendarTableSelect'
import Modal from 'react-awesome-modal';
import UserSeachTableList from 'components/UserSeachTableList';
import * as api from 'lib/api'
import {Button as Btn, Dropdown, Icon, Menu, Input} from "antd";

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show:0,
            visible: false,
            modal: {
                title: "",
                name: "",
                id: "",
                time: ""
            }
        }

    }

    openModal(name) {
        if (name != "") {
            api.getSearchUserList(name).then(response => {
                console.log(response)
                this.setState({
                    tables: response.data.data
                })
            });
        }

        this.setState({
            visible: true,
            modal: {
                title: "1234",
                name: "1234",
                id: "1234",
                time: "1234"
            }
        });
    }

    selectSearch(onsubmit) {
        // onsubmit();
        this.closeModal();
    }

    closeModal() {
        this.setState({
            visible: false
        });
    }

    _showCalTable(value){
        this.setState({
            show:value
        });
        console.log("@@@@@@@@@@@누름")
        console.log(this.state.show);
    }

    handleChange = (e) => {
        const {onChangeInput} = this.props;
        const {value, name} = e.target;
        onChangeInput({name, value});
    }

    render() {
        const {handleChange} = this
        const { Search } = Input;
        const {startdate, enddate, name, onSubmit, show} = this.props;
        const {onClick} = this.props;

        const menu = (
            <Menu onClick={handleChange}>
                <Menu.Item key="all">전체</Menu.Item>
                <Menu.Item key="nomal">정상</Menu.Item>
                <Menu.Item key="late">지각</Menu.Item>
                <Menu.Item key="error">에러</Menu.Item>
            </Menu>
        );



        return (
            <div>
                <Row>
                    <Col xs lg="1">
                        출근 기간
                    </Col>
                    <Col xs lg="2">
                        <Input
                            title="시작 날짜"
                            type="date"
                            value={startdate}
                            onChange={handleChange}
                            name="startdate"
                        />
                    </Col>
                    ~
                    <Col xs lg="2">
                        <Input
                            title="끝 날짜"
                            type="date"
                            value={enddate}
                            onChange={handleChange}
                            name="enddate"
                        />
                    </Col>
                    <Col xs lg="3">
                        <Search placeholder="사원 이름" onSearch={(value) => this.openModal(value)}/>
                    </Col>
                    <Col>
                        <Button onClick={() => onSubmit()} theme="outline">검색</Button>
                    </Col>
                </Row>

                <Row>
                    <Col xs lg="1">
                        근무 상태
                    </Col>
                    <Col>
                        <Dropdown overlay={menu}>
                            <Btn style={{marginLeft: 8}}>
                                전체 <Icon type="down"/>
                            </Btn>
                        </Dropdown>
                    </Col>
                </Row>


                <CalendarTableSelect show={show} show2={(a)=>this._showCalTable(a)}/>
                <Modal visible={this.state.visible} width="400" height="400" effect="fadeInUp"
                       onClickAway={() => this.closeModal()}>
                    <div className="Modal"><br/>
                        {this.state.tables ?
                            <UserSeachTableList tables={this.state.tables} onClick={onClick} onSubmit={onSubmit}
                                                modalClose={() => this.closeModal()}/> : "이름을 입력하세요"}
                        &nbsp;&nbsp;&nbsp;
                        <button onClick={() => this.closeModal()}>닫기</button>
                    </div>
                </Modal>
            </div>
        );
    }


}

export default Search;