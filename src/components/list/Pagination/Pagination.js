import React, {Component} from 'react';
import {Pagination, Row, Col} from "antd";
import "antd/dist/antd.css";

class ListPagination extends Component {

    handlePageChange = (pageNumber) => {
        const {onChange} = this.props;

        onChange(pageNumber);
    }

    render(){
        const {handlePageChange} = this;
        const {activePage, totalCount} = this.props;

        console.log("현재 activePage : " + activePage);
        console.log("현재 totalCount : " + totalCount);

        return(
            <div>
                <Row type="flex" justify="center">
                    <Pagination defaultCurrent={activePage} current={activePage} total={totalCount} onChange={handlePageChange} pageSize={30}/>
                </Row>
            </div>
        );
    }
}

export default ListPagination;