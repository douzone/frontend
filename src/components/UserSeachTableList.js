import React from 'react';
import { Table } from 'react-bootstrap';
import './UserSeachTableList.css';



const UserSeachTableList = ({tables,onClick,onSubmit,modalClose}) => {
    const _onclick =(no) => function(e){
        onClick(no); 
        onSubmit(no);
        modalClose();
        
     }
    const UserSeachTableList =
      tables.map(
            (table, index) => {
                 return (
                    <tr key={index} onClick={_onclick(table.no)}>
                        <td>{table.no}</td>
                        <td>{table.username}</td>   
                        <td>{table.name}</td>
                        <td>{table.role}</td>
                    </tr>
                )
            }
        );
    return (
        <div id="modal-table">
            <Table responsive hover>
                <thead>
                <tr>
                    <th> No</th>
                    <th>아이디</th>
                    <th>이름</th>
                    <th>권한</th>
                </tr>
                </thead>
                <tbody>
                    { UserSeachTableList }
                </tbody>
            </Table>
        </div>
    )
    
};

export default UserSeachTableList;
