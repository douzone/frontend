import React, { Component } from "react";
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.css';
import './Calendar.css';
import Modal from 'react-awesome-modal';

// axios.defaults.headers.common['Authorization'] = 
//                                 'Bearer ' + localStorage.getItem('jwtToken');

// axios.defaults.headers.common['Authorization'] = 
// 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTU2NTAyNDE2LCJpYXQiOjE1NTU4OTc2MTZ9.hK9ujazIT2XpBxLn_kL_gzPEfssICpOurzI8I43D-zkTge0g7MwIReZ_B9y1jNe94gqw71OTnYOliT3AN2ZRbA';
class Calendar extends Component {

    constructor(props) {
      

        super(props);
        
        this.state = {
            date:new Date(),
            events:[],
            visible : false,
            modal:{
                title:"",
                name:"",
                id:"",
                time:""
            }
        }
    }
    openModal(info) {
        
        this.setState({
            visible : true,
            modal:{
                title:info.title,
                 name:info.name,
                 id:info.id,
                 time:info.time
            }
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }

    render() {
        let ary= null
       const{calList} = this.props;
       if(calList!=null){
        ary= calList.data.map((items,index)=>{
        return {title:items.state+" "+items.commute, start: items.day,user_no:items.user_no,commute_no:items.commute_no,name:items.name,id:items.id,commute:items.commute,time:items.day}
        });}
        return (
            <div id="example-component">
                <FullCalendar
                    id = "your-custom-ID"
                    header = {{
                        left: 'prev,next today myCustomButton',
                        center: 'title',
                        right: 'month,basicWee(k,basicDay'
                    }}
                    defaultDate={this.state.date}
                    locale={'ko'}
                    navLinks= {true} // can click day/week names to navigate views
                    editable= {true}
                    eventLimit= {true} // allow "more" link when too many events
                    titleFormat={'YYYY년 MM월'}
                    events = {ary}
                    timeFormat = {"HH:mm"}
                    eventClick={ (info)=>{
                        console.log(info);
                        return this.openModal(info)
                        
                    }
                        // alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
                        // alert('View: ' + info.view.type);

                        //클릭시 red
                        //info.el.style.borderColor = 'red';
                    }
                />
               <Modal visible={this.state.visible} width="300" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div className = "Modal"><br/>
                        <h1>{this.state.modal.title}</h1>
                        <br/>
                        <p>이름: {this.state.modal.name}</p>
                        <p>아이디: {this.state.modal.id}</p>
                        <p>시간 : {this.state.modal.time}</p>
                        <a href=" " onClick={() => this.closeModal()}>수정하기</a>
                        &nbsp;&nbsp;&nbsp;
                        <a href=" " onClick={() => this.closeModal()}>닫기</a>
                    </div>
                </Modal>
            </div>
        );
    }

}




export default Calendar;