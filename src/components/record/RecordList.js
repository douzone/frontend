import React from 'react';
import {Table, Badge} from 'react-bootstrap';

const RecordList = ({tables, badge}) => {

    const RecordList = tables.map(
        (table, index) => {
            const {day, actor, id, recordType, content, read} = table.toJS();

            if(read == true){
                badge = "success";
            }
            else{
                badge = "danger";
            }
            return (
                <tr key={index}>
                    <td>{day}</td>
                    <td>{actor}</td>
                    <td>{id}</td>
                    <td>{recordType}</td>
                    <td>{content}</td>
                    <td><h6><Badge variant={badge}>{String(read)}</Badge></h6></td>
                </tr>
            );
        }
    );

    return (
        <div>
            <Table responsive hover>
                <thead>
                <tr>
                    <th>날짜</th>
                    <th>활동 행위자</th>
                    <th>아이디</th>
                    <th>타입</th>
                    <th>내용</th>
                    <th>읽음 여부</th>
                </tr>
                </thead>
                <tbody>
                {RecordList}
                </tbody>
            </Table>
        </div>
    );
}

export default RecordList;