import React, {Component} from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import {Button as Btn, Dropdown, Icon, Input, Menu} from 'antd';
import CalendarTableSelect from "../list/Search/CalendarTableSelect";
import Modal from "react-awesome-modal";


class RecordSearch extends Component {

    handleChange = (e) => {
        const {onChangeInput} = this.props;
        const {value, name} = e.target;

        onChangeInput({name, value});
    }

    handleSubmit = () => {
        const {onSubmit} = this.props;

        onSubmit();
    }

    render() {
        const {startdate, enddate, name, content} = this.props;
        const {Search} = Input;

        /*
        const menu = (
            <Menu>
                <Menu.item key="all">전체</Menu.item>
                <Menu.item key="출장">출장</Menu.item>
                <Menu.item key="외근">외근</Menu.item>
                <Menu.item key="연차">연차</Menu.item>
                <Menu.item key="반차">반차</Menu.item>
                <Menu.item key="교욱">교육</Menu.item>
            </Menu>
        );
         */

        return (
            <div>
                <Row>
                    <Col xs lg="1">
                        활동 기간
                    </Col>
                    <Col xs lg="2">
                        <Input
                            title="시작 날짜"
                            type="date"
                            value={startdate}
                            name="startdate"
                            onChange={this.handleChange}
                        />
                    </Col>
                    ~
                    <Col xs lg="2">
                        <Input
                            title="끝 날짜"
                            type="date"
                            value={enddate}
                            name="enddate"
                            onChange={this.handleChange}
                        />
                    </Col>
                    <Col xs lg="3">
                        <Search placeholder="사원 이름" name="name" onSearch={(value) => this.openModal(value)} onChange={this.handleChange}/>
                    </Col>
                    <Col>
                        <Input
                            placeholder="내용"
                            title="content"
                            type="text"
                            value={content}
                            name="content"
                            onChange={this.handleChange}
                        />
                    </Col>
                    <Col>
                        <Button onClick={this.handleSubmit} theme="outline">검색</Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default RecordSearch;