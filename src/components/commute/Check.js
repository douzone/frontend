import React, {Component} from 'react';
import Clock from 'react-live-clock';
import {Link} from 'react-router-dom';
import {Button, Row, Col, Container} from 'react-bootstrap';

class Check extends Component {

    render() {
        const {goto, gooff, gotoDisabled, gotoVariant, offDisabled, offVariant, onGoTo, onGoOff} = this.props;

        return (
            <Container>
                <Row className="justify-content-md-center">
                    <Col md="auto">
                        <div>
                            <h1 className="h3 mb-1 text-gray-800">
                                <Clock format={"HH:mm:ss A MM/DD"} ticking={true}/>
                            </h1>

                            <Button variant={gotoVariant} size="lg" disabled={gotoDisabled} onClick={() => {
                                console.log("출근 버튼 클릭");
                                onGoTo();
                                // this.goTo();
                            }}>
                                출근
                            </Button>

                            <Button variant={offVariant} size="lg" disabled={offDisabled} onClick={() => {
                                console.log("퇴근 버튼 클릭");
                                onGoOff();
                                // this.goOff();
                            }}>
                                퇴근
                            </Button>

                            <br/><br/><br/><br/><br/>

                            <Link to="list">
                                <Button variant="primary" size="lg" onClick={() => {

                                }}>
                                    출퇴근 조회
                                </Button>
                            </Link>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Check;