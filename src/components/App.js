import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Navigation from '../nav/Navigation';
import Error404 from '../error/404';
import Jojeong from './Jojeong';

import Footer from '../footer/Footer';
import Container from '../Container';
import Header from '../header/Header';

import { ListPage, WorkTimePage, BreakTimePage, CommuteCheckPage, HoliDayPage, AddPage, RecordPage } from 'pages';

import { Setting, Time } from '../commute';

const App = () => (
        <div id='wrapper'>
            <Navigation/>
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <Header />
                    <Switch>
                        {/* 컨테이너 파일 작성해서 추가하시면 됩니다. */}
                        <Route exact path="/" component={Container} />
                        <Route path="/list" component={ListPage} />
                        <Route path="/worktime" component={WorkTimePage} />
                        <Route path="/breaktime" component={BreakTimePage} />
                        <Route path="/holiday" component={HoliDayPage} />
                        <Route path="/setting" component={Setting} />
                        <Route path="/check" component={CommuteCheckPage} />
                        <Route path="/time" component={Time} />
                        <Route path="/Jojeong" component={Jojeong} />
                        <Route path="/Add" component={AddPage} />
                        <Route path="/record" component={RecordPage} />
                        <Route component={Error404} />
                    </Switch>
                </div>
                <Footer />
            </div>
        </div>
);

export default App;