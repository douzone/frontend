import React, {Component} from "react";
import {Button, Row, Col, Form} from "react-bootstrap"
import { Input } from 'antd';

//import Clock from "react-live-clock"

class Add extends Component {

    handleChange = (e) => {
        const {onChangeInput} = this.props;
        const {value, name} = e.target;
        onChangeInput({name, value});
    }

    render() {
        const {handleChange} = this;
        const { TextArea } = Input;

        const {
            name,
            startDay,
            endDay,
            title,
            gubun,
            content,
            onSubmit
        } = this.props;

        return (
            <div classname="container-fluid">
                신청 시간 및 날짜 :{" "}
                {
                    // <Clock
                    //   format={"YYYY 년 MM 월 DD 일 HH:mm:ss"}
                    //   ticking={true}
                    //   timezone={"KR/Pacific"}
                    // />
                }
                <br/>

                <Row>
                    <Col xs lg="1">
                        이름 :{" "}
                    </Col>
                    <Col xs lg="2">
                        <Form.Control
                            placeholder="홍길동"
                            type="text"
                            value={name}
                            onChange={handleChange}
                            name="name"
                        />
                    </Col>
                </Row>

                <br/>

                <Row>
                    <Col xs lg="1">
                        기간 :{" "}
                    </Col>
                    <Col xs lg="2">
                        <Form.Control
                            type="date"
                            value={startDay}
                            onChange={handleChange}
                            name="startDay"
                        />
                    </Col>
                    ~
                    <Col xs lg="2">
                        <Form.Control
                            type="date"
                            value={endDay}
                            onChange={handleChange}
                            name="endDay"
                        />
                    </Col>
                </Row>

                <br/>

                <Row>
                    <Col xs lg="1">
                        제목 :{" "}
                    </Col>
                    <Col xs lg="5">
                        <Form.Control type="text" value={title} onChange={handleChange} name="title"/>
                    </Col>
                </Row>

                <br/>

                <Row>
                    <Col xs lg="1">
                        근태구분 :{" "}
                    </Col>
                    <Col>
                        <select name="gubun" type="text" value={gubun} onChange={handleChange}>
                            <option value="출장">출장</option>
                            <option value="외근">외근</option>
                            <option value="연차">연차</option>
                            <option value="반차">반차</option>
                            <option value="교육">교육</option>
                        </select>
                    </Col>
                </Row>

                <br/>

                <Row>
                    <Col xs lg="1">
                        근태 사유 :{" "}
                    </Col>
                    <Col>
                        <TextArea name='content' rows={4} onChange={handleChange} value={content} />
                    </Col>
                </Row>
                        <br/>
                        <br/>
                        <Button onClick={onSubmit} theme="outline">
                            등록
                        </Button>
                        <div/>
            </div>
    );
    }
    }

    export default Add;
