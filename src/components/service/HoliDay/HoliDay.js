import React, { Component } from 'react';
import { Button as Btn, Menu, Icon, Dropdown, message, Input } from 'antd';
import {Button} from 'react-bootstrap';

class HoliDay extends Component {

    UseChange = (e) => {
        const { onChangeInput } = this.props;
        const name = 'use';
        const value = e.key;
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const { onChangeInput } = this.props;
        const { value, name } = e.target;
        onChangeInput({name, value});
    }
    
    submitCheck = () => {
        const { day, description, onSubmit } = this.props;

        if(day <= 0){
            message.info('휴일날짜를 선택하세요.');
            return;
        }

        if(description <= 0){
            message.info('휴게설명에 내용이 없습니다.');
            return;
        }

        onSubmit();
    }

    render() {
        const { UseChange, handleChange, submitCheck } = this;
        const { TextArea } = Input;
        const { day, use, description } = this.props;

        const menu = (
            <Menu onClick={UseChange}>
              <Menu.Item key="true">true</Menu.Item>
              <Menu.Item key="false">false</Menu.Item>
            </Menu>
          );
        
        return (
            <div>
                <div>
                    <label>휴일날짜</label>&nbsp;
                    <input
                        title="휴일날짜"
                        type="date"
                        value={day}
                        onChange={handleChange}
                        name="day"
                    />
                </div>

                <div>
                    <label>사용여부</label>
                    <Dropdown overlay={menu} >
                        <Btn style={{ marginLeft: 8 }} >
                            {use} <Icon type="down" />
                        </Btn>
                    </Dropdown>
                </div>

                <div>
                    <label>휴일설명</label>
                    <TextArea name='description' rows={4} onChange={handleChange} value={description} />
                </div>

                <Button onClick={submitCheck} theme="outline">등록</Button>
            </div>
        );
    }
}

export default HoliDay;
