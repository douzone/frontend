import React, { Component } from 'react';
import { TimePicker, Button as Btn, Menu, Icon, Dropdown, message, Input } from 'antd';
import {Button} from 'react-bootstrap';
import moment from 'moment';

class WorkTime extends Component {

    startTimeHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'start';
        onChangeInput({name, value});
    }

    endTimeHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'end';
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const { onChangeInput } = this.props;
        const name = 'use';
        const value = e.key;
        onChangeInput({name, value});
    }

    textAreaChange = (e) => {
        const { onChangeInput } = this.props;
        const { value, name } = e.target;
        onChangeInput({name, value});
    }
    
    submitCheck = () => {
        const { start, end, description, onSubmit } = this.props;
        const format = 'HH:mm';
        const between = moment.duration(moment(start, format).diff(moment(end, format))).asMinutes();
        if(between >= 0){
            message.info('시작시간과 끝시간의 간격이 없습니다.');
            return;
        }

        if(description <= 0){
            message.info('휴게설명에 내용이 없습니다.');
            return;
        }

        onSubmit();

    }

    render() {
        const { startTimeHandleChange, endTimeHandleChange, handleChange, submitCheck, textAreaChange } = this;
        const { TextArea } = Input;
        const { start, end, use, description } = this.props;
        const format = 'HH:mm';

        const menu = (
            <Menu onClick={handleChange}>
              <Menu.Item key="true">true</Menu.Item>
              <Menu.Item key="false">false</Menu.Item>
            </Menu>
          );
        
        return (
            <div>
                <div>
                    <label>시작시간</label>&nbsp;
                    <TimePicker
                        format={format}
                        value={moment(start, format)}
                        allowClear={false}
                        onChange={startTimeHandleChange}
                    />
                </div>
                <div>
                    <label>끝시간</label>&nbsp;
                    <TimePicker
                        format={format}
                        value={moment(end, format)}
                        allowClear={false}
                        onChange={endTimeHandleChange}
                    />
                </div>
                <div>
                    <label>사용여부</label>
                    <Dropdown overlay={menu} >
                        <Btn style={{ marginLeft: 8 }} >
                            {use} <Icon type="down" />
                        </Btn>
                    </Dropdown>
                </div>
                <div>
                    <label>휴게설명</label>
                    <TextArea name='description' rows={4} onChange={textAreaChange} value={description} />
                </div>
                <Button onClick={submitCheck} theme="outline">등록</Button>
            </div>
        );
    }
}

export default WorkTime;
