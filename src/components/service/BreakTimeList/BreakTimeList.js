import React from 'react';
import { Table } from 'react-bootstrap';

const BreakTimeList = ({tables}) => {
    const BreakTimeList = tables.map(
        (table, index) => {
            const { start, end, description, use } = table.toJS();
            return (
                <tr key={index}>
                    <td>{start}</td>
                    <td>{end}</td>
                    <td>{description}</td>
                    <td>{String(use)}</td>
                </tr>
            )
        }
    );
    return (
        <div>
            <Table responsive hover>
                <thead>
                <tr>
                    <th>시작 시간</th>
                    <th>끝 시간</th>
                    <th>휴게설명</th>
                    <th>사용유무</th>
                </tr>
                </thead>
                <tbody>
                    { BreakTimeList }
                </tbody>
            </Table>
        </div>
    )
    
};


export default BreakTimeList;