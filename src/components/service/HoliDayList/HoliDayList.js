import React from 'react';
import { Table } from 'react-bootstrap';

const HoliDayList = ({tables}) => {
    const HoliDayList = tables.map(
        (table, index) => {
            const { day, description, use } = table.toJS();
            return (
                <tr key={index}>
                    <td>{day}</td>
                    <td>{description}</td>
                    <td>{String(use)}</td>
                </tr>
            )
        }
    );
    return (
        <div>
            <Table responsive hover>
                <thead>
                <tr>
                    <th>휴일 날짜</th>
                    <th>휴일 설명</th>
                    <th>사용유무</th>
                </tr>
                </thead>
                <tbody>
                    { HoliDayList }
                </tbody>
            </Table>
        </div>
    )
    
};


export default HoliDayList;