import React from 'react';
import { Table } from 'react-bootstrap';

const WorkTimeList = ({tables}) => {
    const WorkTimeList = tables.map(
        (table, index) => {
            const { start, end, use } = table.toJS();

            return (
                <tr key={index}>
                    <td>{start}</td>
                    <td>{end}</td>
                    <td>{String(use)}</td>
                </tr>
            )
        }
    );
    return (
        <div>
            <Table responsive hover>
                <thead>
                <tr>
                    <th>시작 시간</th>
                    <th>끝 시간</th>
                    <th>사용유무</th>
                </tr>
                </thead>
                <tbody>
                    { WorkTimeList }
                </tbody>
            </Table>
        </div>
    )
    
};


export default WorkTimeList;