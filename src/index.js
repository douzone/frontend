import React from "react";
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import Root from './root';
import { Provider } from 'react-redux';
import configure from 'store/configure';

const store = configure();

ReactDOM.render(
    <Provider store={store}>    
        <Root />
    </Provider>,
document.getElementById('root'));
    
serviceWorker.unregister();