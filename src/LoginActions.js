import { AUTH_LOGIN, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILURE, AUTH_LOGOUT } from './LoginActionTypes';
import axios from 'axios';


// thunk 함수 : API와 통신하는 함수
export function loginRequest(username, password) {
    return async (dispatch) => {
        // 로그인이 시작됨을 알리는 action 객체를 reducer에게 전달
        dispatch(login());
        // backend와 통신
        return await axios.post('http://localhost:8076/user/auth', 
        { username, password })
        .then((response) => {
            // 로그인 성공시 성공을 알리는 action 객체를 reducer에게 전달
            if(response.data.result === 'fail') {
                dispatch(loginFailure());
            } 
            else if (response.data.token){
                dispatch(loginSuccess(username));
                //localStorage에 토큰 등록
                localStorage.setItem('user', JSON.stringify(response.data.token));
                console.log(localStorage.getItem('user'));
            } 
            else {
                dispatch(loginFailure());
            }
        }).catch((error) => {
            // 로그인 실패시 실패를 알리는 action 객체를 reducer에게 전달
            dispatch(loginFailure());
        });
    };
}

export function logoutRequest() {
    console.log('로그아웃');
    return (dispatch) => {
        dispatch(logout());
        localStorage.removeItem('user');
    }
}

// 액션 생성자 함수로 액션 객체를 리턴
export function login() {
    console.log('login 액션 생성자 함수 실행중');
    console.log('');
    return {
        type: AUTH_LOGIN
    }
}

export function loginSuccess(id) {
    console.log('loginSuccess 액션 생성자 함수 실행중');
    console.log('');
    return {
        type: AUTH_LOGIN_SUCCESS,
        id
    };
}

export function loginFailure() {
    console.log('loginFailure 액션 생성자 함수 실행중');
    console.log('');
    return {
        type: AUTH_LOGIN_FAILURE
    };
}

export function logout() {
    return {
        type: AUTH_LOGOUT
    };
}
