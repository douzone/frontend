import React, { Component } from 'react';
import Search from 'components/list/Search';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as listActions from 'store/modules/list';
import * as getCalList from 'store/modules/calendarlist';
import * as paginationList from 'store/modules/pagination';

class ListSearchContainer extends Component {

    handleClick = (value) => {
        const {ListActions} = this.props;
        ListActions.click(value);
    }

    handleChangeInput = ({name, value}) => {
        
        const { ListActions } = this.props;
        ListActions.changeInput({name, value});
    }

    handleSubmit = async (paramNo) => {
        
        const { startdate, enddate, name, activePage, ListActions, GetCalList, PaginationActions } = this.props;
        
        const search = {
            startdate,
            enddate,
            name,
            paramNo
        }

        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await PaginationActions.getSearchPage(startdate, enddate, paramNo);
            await ListActions.getTableList(search, pageNumber);
            if(search.paramNo==null)
             search.paramNo=0;
             console.log(search.paramNo);
            await GetCalList.getList(search);
          } catch (e) {
            console.log(e);
          }
    }

    render() {
        const { startdate, enddate, name,no,show } =this.props;
        const { handleChangeInput, handleSubmit } = this;
        return (
            <Search
                startdate={startdate}
                enddate={enddate}
                name={name}
                onChangeInput={handleChangeInput}
                onSubmit={handleSubmit}
                onClick={this.handleClick}
                no ={no}
                show={show}
            />
        );
    }
}

//구독
export default connect(
    (state) => ({
        startdate: state.list.get('startdate'),
        enddate: state.list.get('enddate'),
        name: state.list.get('name'),
        no: state.list.get('no'),
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch),
        GetCalList: bindActionCreators(getCalList, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch)
    })
)(ListSearchContainer);