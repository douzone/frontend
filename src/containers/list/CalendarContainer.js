import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as listActions from 'store/modules/calendarlist';
import Calendar  from 'components/calendar/Calendar';


class CalendarContainer extends Component {

    render() {
        const { calList, loading } =this.props;
        if(loading) return null;
        return (
            <Calendar
        calList={calList}
          />
        );
    }
}


export default connect(
    (state) => (
        {
        calList: state.calendarlist.get('calList'),
        loading: state.pender.pending['list/GET_CALENDAR_LIST']
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch)
    })
)(CalendarContainer);