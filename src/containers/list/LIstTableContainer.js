import React, { Component } from 'react';
import TableList from 'components/list/TableList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as listActions from 'store/modules/list';

class LIstTableContainer extends Component {

    /*
    getPostList = () => {
        // 페이지와 태그 값을 부모로부터 받아 옵니다.
        const { activePage, ListActions } = this.props;
        
        const search = {
            startdate : '2019-04-25',
            enddate : '2019-04-25',
            name : '박성혜'
        }
        const page = activePage;
        console.log(search);
        ListActions.getTableList(search, page);
      }
    
    componentDidMount() {
    this.getPostList();
    }
    */

    getFullList = async() => {
        const {activePage, ListActions} = this.props;

        try {
            await ListActions.getFullTableList(activePage);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getFullList();
    }

    render() {
        const { tables, loading } =this.props;
        if(loading && tables <= 0) return null;
        return (
            <div>
                <TableList tables={tables}/>      
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.list.get('tables'),
        loading: state.pender.pending['list/FULL_TABLE_LIST'],
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch)
    })
)(LIstTableContainer);