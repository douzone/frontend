import React, {Component} from 'react';
import Pagination from 'components/list/Pagination';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as listActions from 'store/modules/pagination';
import * as tableActions from 'store/modules/list';


class PaginationContainer extends Component {

    handlePageChange = (pageNumber) =>{
        const {activePage, ListActions} = this.props;

        ListActions.pageChange({activePage, pageNumber});
        this.handleList(pageNumber);
    }

    handlePage = async() => {
        const {ListActions} = this.props;

        try {
            await ListActions.getPage();
        } catch (e) {
            console.log(e);
        }
    }

    handleList = async(pageNumber) => {
        const {TableActions, startdate, enddate, name, no} = this.props;
        const paramNo = no;

        if(startdate == '' && enddate == '' && name == '') {
            try {
                console.log("change page : " + pageNumber);
                await TableActions.getFullTableList(pageNumber);
            } catch (e) {
                console.log(e);
            }
        }
        else{
            try{
                const search = {startdate, enddate, name, paramNo};

                console.log("start date : " + startdate);
                console.log("end date : " + enddate);
                console.log("name : " + name);
                console.log("paramNo : " + paramNo);
                console.log("pageNumber : " + pageNumber);

                await TableActions.getTableList(search, pageNumber);
            }
            catch(e){
                console.log(e);
            }
        }
    }

    componentWillMount() {
        this.handlePage();
    }

    componentDidMount() {
        const { ListActions } = this.props;

        ListActions.initialize();
    }

    render(){
        const {activePage, totalCount} = this.props;

        return(
            <div>
                <Pagination activePage={activePage} totalCount={totalCount} onChange={this.handlePageChange}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get("activePage"),
        totalCount: state.pagination.get("totalCount"),
        startdate: state.list.get("startdate"),
        enddate: state.list.get("enddate"),
        name: state.list.get("name"),
        no: state.list.get("no"),
        tables: state.list.get("tables")
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch),
        TableActions: bindActionCreators(tableActions, dispatch)
    })
)(PaginationContainer);