import React, { Component } from 'react';
import Check from 'components/commute/Check';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as commuteActions from 'store/modules/commute';

class CommuteCheckContainer extends Component {

    handleGoTo = async() => {
        const { CommuteActions } = this.props;

        try{
            await CommuteActions.goTo(1, '정영석', '출근', '출근 입력');
        } catch(e){
            console.log(e);
        }
    }

    handleGoOff = async() => {
        const { CommuteActions } = this.props;

        try{
            await CommuteActions.goOff(1, '정영석', '퇴근', '퇴근 입력');
        } catch(e){
            console.log(e);
        }
    }

    handleState = () => {
        //const { goto, gooff, gotoDisabled, gotoVariant, offDisabled, offVariant } = this.props;
        const goto = localStorage.getItem('goto');
        const gooff = localStorage.getItem('gooff');

        console.log(goto);
        console.log(gooff);

        if(goto == false || goto == null){
            //this.convertState('gooff', true);
            this.convertState('gotoDisabled', false);
            this.convertState('gotoVariant', 'primary');
            this.convertState('offDisabled', true);
            this.convertState('offVariant', 'secondary');
        }
        else{
            //this.convertState('gooff', false);
            this.convertState('gotoDisabled', true);
            this.convertState('gotoVariant', 'secondary');
            this.convertState('offDisabled', false);
            this.convertState('offVariant', 'primary');
        }
    }

    convertState(name, value){
        const { CommuteActions } = this.props;

        CommuteActions.stateChange({name, value});
    }

    componentWillMount() {
        this.handleState();
    }

    render(){
        const {gotoDisabled, gotoVariant, offDisabled, offVariant, goto, gooff} = this.props;

        return(
            <div>
                <Check goto={localStorage.getItem('goto')} gooff={localStorage.getItem('gooff')} gotoDisabled={gotoDisabled} gotoVariant={gotoVariant} offDisabled={offDisabled} offVariant={offVariant} onGoTo={this.handleGoTo} onGoOff={this.handleGoOff} />
            </div>
        );
    }
}

export default connect(
    (state) => ({
        goto: state.commute.get("goto"),
        gooff: state.commute.get("gooff"),
        gotoDisabled: state.commute.get("gotoDisabled"),
        gotoVariant: state.commute.get("gotoVariant"),
        offDisabled: state.commute.get("offDisabled"),
        offVariant: state.commute.get("offVariant"),
        no: state.list.get('no')
    }),
    (dispatch) => ({
        CommuteActions: bindActionCreators(commuteActions, dispatch)
    })
)(CommuteCheckContainer);