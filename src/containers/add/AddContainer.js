import React, { Component } from "react";
import Add from "components/add/Add";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as addActions from "store/modules/add"; 

class AddContainer extends Component {
  componentDidMount() {
    const { AddActions } = this.props;
    AddActions.initialize(); // 에디터를 초기화합니다.
  }
  handleChangeInput = ({ name, value }) => {
    const { AddActions } = this.props;
    AddActions.changeInput({ name, value });
  };

  handleSubmit = async () => {

    const {
      name,
      startDay,
      endDay,
      title,
      gubun,
      content,
      AddActions
    } = this.props;
    //const userId = "aabb";
    const add = {
      name,
      startDay,
      endDay,
      title,
      state: '정상',
      workAttitudeList: gubun,
      userNo: 1011,
      userId: 'JYD',
      content,
      no: 1
    };
    try {
      await AddActions.writeAdd(add);
      AddActions.initialize(); // 에디터를 초기화합니다.
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { handleChangeInput, handleSubmit } = this;
    const { name, startDay, endDay, title, gubun, content } = this.props;
    return (
      <Add
        name={name}
        startDay={startDay}
        endDay={endDay}
        title={title}
        gubun={gubun}
        content={content}
        onChangeInput={handleChangeInput}
        onSubmit={handleSubmit}
      />
    );
  }
}

export default connect(
  state => ({
    name: state.add.get("name"),
    startDay: state.add.get("startDay"),
    endDay: state.add.get("endDay"),
    title: state.add.get("title"),
    gubun: state.add.get("gubun"),
    content: state.add.get("content")
  }),
  dispatch => ({
    AddActions: bindActionCreators(addActions, dispatch)
  })
)(AddContainer);
