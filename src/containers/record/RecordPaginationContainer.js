import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Pagination from 'components/list/Pagination';
import * as paginationActions from 'store/modules/pagination';
import * as recordActions from 'store/modules/record';


class RecordPaginationContainer extends Component {

    handlePageChange = (pageNumber) => {
        const { activePage, PaginationActions } = this.props;

        PaginationActions.pageChange({activePage, pageNumber});
        this.handleList(pageNumber);
    }

    handleList = async(pageNumber) => {
        const { RecordActions, startdate, enddate, name, content } = this.props;

        if(startdate == "" && enddate == "" && name == "" && content == "") {
            try {
                await RecordActions.getRecordList(pageNumber);
            } catch (e) {
                console.log(e);
            }
        }
        else {
            try{
                await RecordActions.getSearchRecordList(pageNumber, startdate, enddate, name, content);
            } catch(e){
                console.log(e);
            }
        }
    }

    handlePage = async() => {
        const { PaginationActions } = this.props;

        try{
            await PaginationActions.getRecordTotal();
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.handlePage();
    }

    componentDidMount() {
        const { PaginationActions } = this.props;

        PaginationActions.initialize();
    }

    render() {
        const {activePage, totalCount} = this.props;

        return(
            <div>
                <Pagination activePage={activePage} totalCount={totalCount} onChange={this.handlePageChange}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get('activePage'),
        totalCount: state.pagination.get('totalCount'),
        startdate: state.record.get('startdate'),
        enddate: state.record.get('enddate'),
        name: state.record.get('name'),
        content: state.record.get('content')
    }),
    (dispatch) => ({
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        RecordActions: bindActionCreators(recordActions, dispatch)
    })
)(RecordPaginationContainer);