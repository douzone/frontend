import React, { Component } from 'react';
import RecordList from 'components/record/RecordList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as recordActions from 'store/modules/record';


class RecordListContainer extends Component {

    getList = async() => {
        const { RecordActions, activePage } = this.props;

        try{
            await RecordActions.getRecordList(activePage);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        const {tables} = this.props;

        this.getList();
    }

    render(){
        const { tables, loading, badge } = this.props;
        if(loading && tables <= 0) return null;

        return(
            <div>
                <RecordList tables={tables} badge={badge}/>
            </div>
        );
    }
}


export default connect(
    (state) => ({
        tables: state.record.get('tables'),
        badge: state.record.get('badge'),
        loading: state.pender.pending['record/RECORD_LIST'],
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        RecordActions: bindActionCreators(recordActions, dispatch)
    })
)(RecordListContainer);