import React, {Component} from 'react';
import RecordSearch from 'components/record/RecordSearch';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as recordActions from 'store/modules/record';
import * as paginationActions from 'store/modules/pagination';

class RecordSearchContainer extends Component {

    handleChangeInput = ({name, value}) => {
        const {RecordActions} = this.props;

        RecordActions.changeInput({name, value});
    }

    handleSubmit = async() => {
        const {startdate, enddate, content, name, activePage, RecordActions, PaginationActions} = this.props;

        const pageNumber = 1;

        PaginationActions.pageChange({activePage, pageNumber});

        try{
            await PaginationActions.getSearchRecordPage(startdate, enddate, name, content);
            await RecordActions.getSearchRecordList(activePage, startdate, enddate, name, content);
        } catch(e){
            console.log(e);
        }
    }

    render(){
        const {startdate, enddate, name, content} = this.props;

        return(
            <div>
                <RecordSearch startdate={startdate} enddate={enddate} name={name} content={content} onSubmit={this.handleSubmit} onChangeInput={this.handleChangeInput}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        startdate: state.record.get("startdate"),
        enddate: state.record.get("enddate"),
        name: state.record.get("name"),
        content: state.record.get("content"),
        activePage: state.pagination.get("activePage")
    }),
    (dispatch) => ({
        RecordActions: bindActionCreators(recordActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(RecordSearchContainer);