import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Pagination from 'components/list/Pagination';
import * as paginationActions from 'store/modules/pagination';
import * as holidayActions from 'store/modules/holiday';


class HoliDayPaginationContainer extends Component {

    handlePageChange = (pageNumber) => {
        const { activePage, PaginationActions } = this.props;

        PaginationActions.pageChange({activePage, pageNumber});
        this.handleList(pageNumber);
    }

    handleList = async(pageNumber) => {
        const { HoliDayActions } = this.props;

        try{
            await HoliDayActions.getHoliDayList(pageNumber);
        } catch(e){
            console.log(e);
        }
    }

    handlePage = async() => {
        const { PaginationActions } = this.props;

        try{
            await PaginationActions.getHoliDayTotal();
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.handlePage();
    }

    componentDidMount() {
        const { PaginationActions } = this.props;

        PaginationActions.initialize();
    }

    render() {
        const { activePage, totalCount } = this.props;

        return(
            <div>
                <Pagination activePage={activePage} totalCount={totalCount} onChange={this.handlePageChange}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get("activePage"),
        totalCount: state.pagination.get("totalCount")
    }),
    (dispatch) => ({
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        HoliDayActions: bindActionCreators(holidayActions, dispatch)
    })
)(HoliDayPaginationContainer);