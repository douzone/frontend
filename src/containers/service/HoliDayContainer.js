import React, {Component} from 'react';
import HoliDay from 'components/service/HoliDay';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as holidayActions from 'store/modules/holiday';
import * as paginationActions from 'store/modules/pagination';

class HoliDayContainer extends Component {

    componentDidMount() {
        const {HoliDayActions} = this.props;
        // HoliDayActions.initialize(); // 에디터를 초기화합니다.
    }

    handleChangeInput = ({name, value}) => {
        const {HoliDayActions} = this.props;
        HoliDayActions.changeInput({name, value});
    }

    handleState(name, value){
        const { HoliDayActions } = this.props;

        HoliDayActions.changeInput({name, value});
    }

    handleSubmit = async () => {

        const {day, use, description, activePage, HoliDayActions, PaginationActions} = this.props;
        const userId = 'admin';
        const holiday = {
            day,
            use,
            description,
            userId
        }

        const pageNumber = 1;

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await HoliDayActions.writeHoliDay(holiday);
            await HoliDayActions.getHoliDayList(pageNumber);
            // HoliDayActions.initialize(); // 에디터를 초기화합니다.
        } catch (e) {
            console.log(e);
        }

        this.handleState('day', '');
        this.handleState('description', '');
        this.handleState('use', 'true');
    }

    render() {
        const {handleChangeInput, handleSubmit} = this;
        const {day, use, description} = this.props;

        return (
            <HoliDay
                day={day}
                use={use}
                description={description}
                onChangeInput={handleChangeInput}
                onSubmit={handleSubmit}
            />
        );
    }
}

export default connect(
    (state) => ({
        day: state.holiday.get('day'),
        use: state.holiday.get('use'),
        description: state.holiday.get('description'),
        activePage: state.pagination.get("activePage")
    }),
    (dispatch) => ({
        HoliDayActions: bindActionCreators(holidayActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(HoliDayContainer);