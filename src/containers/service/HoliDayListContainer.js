import React, { Component } from 'react';
import HoliDayList from 'components/service/HoliDayList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import * as listActions from 'store/modules/list';
import * as holidayActions from 'store/modules/holiday';

class HoliDayListContainer extends Component {
    /*
    getPostList = () => {
        // 페이지와 태그 값을 부모로부터 받아 옵니다.
        const { ListActions } = this.props;
        
        const search = {
            startdate : '2019-04-25',
            enddate : '2019-04-25',
            name : '박성혜'
        }
        const page = 1
        console.log(search);
        ListActions.getTableList(search, page);
      }
    
    componentDidMount() {
    this.getPostList();
    }
    */

    getList = async() => {
        const { HoliDayActions, activePage } = this.props;

        try{
            await HoliDayActions.getHoliDayList(activePage);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getList();
    }

    render() {
        const { tables, loading } =this.props;
        if(loading && tables <= 0) return null;

        return (
            <div>
                <HoliDayList tables={tables}/>      
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.holiday.get('tables'),
        loading: state.pender.pending['holiday/HOLI_DAY_LIST'],
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        HoliDayActions: bindActionCreators(holidayActions, dispatch)
    })
)(HoliDayListContainer);