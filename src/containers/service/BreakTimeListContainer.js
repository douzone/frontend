import React, { Component } from 'react';
import BreakTimeList from 'components/service/BreakTimeList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import * as listActions from 'store/modules/list';
import * as breaktimeActions from 'store/modules/breaktime';

class BreakTimeListContainer extends Component {
    /*
    getPostList = () => {
        // 페이지와 태그 값을 부모로부터 받아 옵니다.
        const { ListActions } = this.props;
        
        const search = {
            startdate : '2019-04-25',
            enddate : '2019-04-25',
            name : '박성혜'
        }
        const page = 1
        console.log(search);
        ListActions.getTableList(search, page);
      }
    
    componentDidMount() {
    this.getPostList();
    }
    */

    getList = async() => {
        const { BreakTimeActions, activePage } = this.props;

        try{
            await BreakTimeActions.getBreakTimeList(activePage);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getList();
    }

    render() {
        const { tables, loading } =this.props;
        if(loading && tables <= 0) return null;

        return (
            <div>
                <BreakTimeList tables={tables}/>      
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.breaktime.get('tables'),
        loading: state.pender.pending['breaktime/BREAK_TIME_LIST'],
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        BreakTimeActions: bindActionCreators(breaktimeActions, dispatch)
    })
)(BreakTimeListContainer);