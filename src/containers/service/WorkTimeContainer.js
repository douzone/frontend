import React, {Component} from 'react';
import WorkTime from 'components/service/WorkTime';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as worktimeActions from 'store/modules/worktime';
import * as paginationActions from 'store/modules/pagination';

class WorkTimeContainer extends Component {

    componentDidMount() {
        const {WorkTimeActions} = this.props;
        //WorkTimeActions.initialize(); // 에디터를 초기화합니다.

    }

    handleChangeInput = ({name, value}) => {
        const {WorkTimeActions} = this.props;
        WorkTimeActions.changeInput({name, value});
    }

    handleSubmit = async () => {

        const {start, end, use, activePage, WorkTimeActions, PaginationActions} = this.props;
        const userId = 'admin';
        const worktime = {
            start,
            end,
            use,
            userId
        }

        const pageNumber = 1;

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await WorkTimeActions.writeWorkTime(worktime);
            await WorkTimeActions.getWorkTimeList(pageNumber);
            //WorkTimeActions.initialize(); // 에디터를 초기화합니다.
        } catch (e) {
            console.log(e);
        }

        this.handleState('start', '00:00');
        this.handleState('end', '00:00');
        this.handleState('use', 'true');
    }

    handleState(name, value){
        const { WorkTimeActions } = this.props;

        WorkTimeActions.changeInput({name, value});
    }

    render() {
        const {handleChangeInput, handleSubmit} = this;
        const {start, end, use} = this.props;
        return (
            <WorkTime
                start={start}
                end={end}
                use={use}
                onChangeInput={handleChangeInput}
                onSubmit={handleSubmit}
            />
        );
    }
}

export default connect(
    (state) => ({
        start: state.worktime.get('start'),
        end: state.worktime.get('end'),
        use: state.worktime.get('use'),
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        WorkTimeActions: bindActionCreators(worktimeActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(WorkTimeContainer);