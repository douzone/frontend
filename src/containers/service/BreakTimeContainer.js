import React, {Component} from 'react';
import BreakTime from 'components/service/BreakTime';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as breaktimeActions from 'store/modules/breaktime';
import * as paginationActions from 'store/modules/pagination';

class BreakTimeContainer extends Component {

    componentDidMount() {
        const {BreakTimeActions} = this.props;
        // BreakTimeActions.initialize(); // 에디터를 초기화합니다.
    }

    handleChangeInput = ({name, value}) => {
        const {BreakTimeActions} = this.props;
        BreakTimeActions.changeInput({name, value});
    }

    handleState(name, value){
        const { BreakTimeActions } = this.props;

        BreakTimeActions.changeInput({name, value});
    }

    handleSubmit = async () => {

        const {start, end, use, description, activePage, BreakTimeActions, PaginationActions} = this.props;
        const userId = 'admin';
        const breaktime = {
            start,
            end,
            use,
            description,
            userId
        }

        const pageNumber = 1;

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await BreakTimeActions.writeBreakTime(breaktime);
            await BreakTimeActions.getBreakTimeList(pageNumber);
            // BreakTimeActions.initialize(); // 에디터를 초기화합니다.
        } catch (e) {
            console.log(e);
        }

        this.handleState('start', '00:00');
        this.handleState('end', '00:00');
        this.handleState('description', '');
        this.handleState('use', 'true');
    }

    render() {
        const {handleChangeInput, handleSubmit} = this;
        const {start, end, use, description} = this.props;
        return (
            <BreakTime
                start={start}
                end={end}
                use={use}
                description={description}
                onChangeInput={handleChangeInput}
                onSubmit={handleSubmit}
            />
        );
    }
}

export default connect(
    (state) => ({
        start: state.breaktime.get('start'),
        end: state.breaktime.get('end'),
        use: state.breaktime.get('use'),
        description: state.breaktime.get('description'),
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        BreakTimeActions: bindActionCreators(breaktimeActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(BreakTimeContainer);