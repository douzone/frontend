import React, { Component } from 'react';
import WorkTimeList from 'components/service/WorkTimeList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import * as listActions from 'store/modules/list';
import * as worktimeActions from 'store/modules/worktime';

class WorkTimeListContainer extends Component {

    /*
    getPostList = () => {
        // 페이지와 태그 값을 부모로부터 받아 옵니다.
        const { ListActions } = this.props;
        
        const search = {
            startdate : '2019-04-25',
            enddate : '2019-04-25',
            name : '박성혜'
        }
        const page = 1
        console.log(search);
        ListActions.getTableList(search, page);
    }
    */

    getList = async() => {
        const { WorkTimeActions, activePage } = this.props;

        try{
            await WorkTimeActions.getWorkTimeList(activePage);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getList();
    }

    componentDidMount() {
        // this.getPostList();
    }

    render() {
        const { tables, loading } =this.props;
        if(loading && tables <= 0) return null;

        return (
            <div>
                <WorkTimeList tables={tables}/>      
            </div>
        );
    }''
}

export default connect(
    (state) => ({
        tables: state.worktime.get('tables'),
        loading: state.pender.pending['worktime/WORK_TIME_LIST'],
        activePage: state.pagination.get('activePage')
    }),
    (dispatch) => ({
        WorkTimeActions: bindActionCreators(worktimeActions, dispatch)
    })
)(WorkTimeListContainer);