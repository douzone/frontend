import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Pagination from 'components/list/Pagination';
import * as paginationActions from 'store/modules/pagination';
import * as worktimeActions from 'store/modules/worktime';


class WorkTimePaginationContainer extends Component {

    handlePageChange = (pageNumber) => {
        const { activePage, PaginationActions } = this.props;

        PaginationActions.pageChange({activePage, pageNumber});
        this.handleList(pageNumber);
    }

    handleList = async(pageNumber) => {
        const { WorkTimeActions } = this.props;

        try{
            await WorkTimeActions.getWorkTimeList(pageNumber);
        } catch(e){
            console.log(e);
        }
    }

    handlePage = async() => {
        const { PaginationActions} = this.props;

        try{
            await PaginationActions.getWorkTimeTotal();
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.handlePage();
    }

    componentDidMount() {
        const { PaginationActions } = this.props;

        PaginationActions.initialize();
    }

    render(){
        const { activePage, totalCount } = this.props;

        return(
            <div>
                <Pagination activePage={activePage} totalCount={totalCount} onChange={this.handlePageChange}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get("activePage"),
        totalCount: state.pagination.get("totalCount")
    }),
    (dispatch) => ({
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        WorkTimeActions: bindActionCreators(worktimeActions, dispatch)
    })
)(WorkTimePaginationContainer);
