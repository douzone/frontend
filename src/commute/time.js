import React from 'react';
import ReactStopwatch from 'react-stopwatch';

const Time = () => {
    return (
        <div className="container-fluid">
            <h1 className="h3 mb-1 text-gray-800">
                현재 근무 시간 조회
            </h1>

            <ReactStopwatch
                seconds={0}
                minutes={0}
                hours={0}
                limit="24:00:00"
                onChange={({ hours, minutes, seconds }) => {
                    console.log(hours + ":" + minutes + ":" + seconds);
                }}
                onCallback={() => console.log('Finish')}
                render={({ formatted }) => {
                    return (
                        <div>
                            <p>
                                { formatted }
                            </p>
                        </div>
                    );
                }}
            />
        </div>
    );
};

export default Time;