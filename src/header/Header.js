import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { logoutRequest } from '../LoginActions';
import { connect } from 'react-redux';
import axios from 'axios';

class Header extends Component {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
  }

    handleLogout = (e) => {
      e.preventDefault();
      return this.props.logoutRequest();
    }

    componentWillMount() {
      // 토큰으로 유저정보 가져와야함
      //this.finduser(localStorage.getItem('user'));
    }

    finduser = async(token) => {
      console.log("finduser" + token)
      let token2 = token.substring(1,token.length).substring(0,token.length-2);
      await axios.get("http://localhost:8076/user", {
        headers : {
          'Authorization' : 'Bearer ' + token2
        }
      })
          .then((response) => {
              console.log("token에 대한 응답값");
              console.log(response);
              })
          .catch((error) => console.log(error));
    }
    
    render() {
      const logoutButton = (
        <Link to="/login" onClick={this.handleLogout} className="btn btn-secondary btn-icon-split">
          <span className="icon text-white-50">
            <i className="fas fa-arrow-right"></i>
          </span>
          <span className="text">logout</span>
          {localStorage.getItem('user')}
        </Link>
      );

      return (
        <div>
          <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3">
            <i className="fa fa-bars"></i>
          </button>

          <ul className="navbar-nav ml-auto">
            <li className="nav-item dropdown no-arrow mx-1">
                <i className="fas fa-bell fa-fw"></i>
                <span className="badge badge-danger badge-counter">3+</span>
            </li>
          </ul>
        </nav>
      </div>
      )
    }
}

const mapStateToProps = (state) => {
  return {
      status: state.authentication.logout.status
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      logoutRequest: () => {
          return dispatch(logoutRequest());
      }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);